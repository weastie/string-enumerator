var EnumString = require('../lib/index.js');

var eString = new EnumString.EnumString('abc');

// Test the next string function
console.log('=======\nTesting the next string function.\n');
var outs = ['a', 'b', 'c', 'aa', 'ab', 'ac', 'ba', 'bb', 'bc', 'ca', 'cb', 'cc', 'aaa'];
var passed = true;
eString.setIndex(0);
var s = '';
for (var i = 0; i < outs.length; i++) {
	s = eString.nextString();
	if (s !== outs[i]) {
		console.log('Testing failed: ' + s + ' should be ' + outs[i]);
		passed = false;
	}
}
if (passed) {
	console.error('Passed!');
} else {
	console.error('Failed!');
}

// Test the previous string function
console.log('=======\nTesting the previous string function.\n');
outs = outs.reverse();
passed = true;
eString.setIndex(eString.getIndex() - 1);
for (i = 0; i < outs.length; i++) {
	s = eString.prevString();
	if (s !== outs[i]) {
		console.log('Testing failed: ' + s + ' should be ' + outs[i]);
		passed = false;
	}
}
if (passed) {
	console.error('Passed!');
} else {
	console.error('Failed!');
}

// Test getting strings at specific indicies
outs = [{in: 0, out: 'a'}, {in: 6, out: 'ba'}, {in: 12, out: 'aaa'}];
passed = true;
console.log('=======\nTesting that getting strings at specified indicies works.\n');
for (i = 0; i < outs.length; i++) {
	eString.setIndex(outs[i].in);
	if (eString.getString() !== outs[i].out) {
		console.log('Testing failed: ' + eString.getString() + ' should be ' + outs[i].out);
		passed = false;
	}
}
if (passed) {
	console.error('Passed!');
} else {
	console.error('Failed!');
}

// Test that the get index from string function works
console.log('=======\nTesting that getting index of a string works.\n');
passed = true;
for (i = 0; i < 300; i++) {
	eString.setIndex(i);
	if (eString.getIndexFromStr(eString.getString()) !== i) {
		console.log('Testing failed: ' + eString.getIndexFromStr(eString.getString()) + ' should be ' + i + ' for string ' + eString.getString());
		passed = false;
	}
}
if (passed) {
	console.error('Passed!');
} else {
	console.error('Failed!');
}
