# string-enumerator

**0 dependencies**

An NPM package for enumerating strings based off of a set of characters. Great for blind brute forcing and fuzzing.

## Example
```js
	var EnumString = require('string-enumerator')
	var charset = 'abcd123';
	var eString = new EnumString.EnumString(charset);

	for (var i = 0; i < 20; i++) {
		console.log(eString.nextString())
	}

	// Logs: a b c d 1 2 3 aa ab ac ad a1 a2 a3 ba bb bc bd b1 b2
```
## API

- [EnumString.EnumString (Constructor)](#enumstring.enumstring)
- [EnumString.prototype.setIndex](#enumstring.prototype.setindex)
- [EnumString.prototype.getIndex](#enumstring.prototype.getindex)
- [EnumString.prototype.getString](#enumstring.prototype.getstring)
- [EnumString.prototype.nextString](#enumstring.prototype.nextstring)
- [EnumString.prototype.prevString](#enumstring.prototype.prevstring)
- [EnumString.prototype.getIndexFromStr](#enumstring.prototype.getindexfromstr)
- [EnumString.letters](#enumstring.letters)
- [EnumString.digits](#enumstring.digits)
- [EnumString.symbols](#enumstring.symbols)

---

## EnumString.EnumString
EnumString.EnumString is the main constructor for the EnumString object, and must
be instatiated to utilize this package. The constructor requires that a string
or array of characters is passed as the first argument, which dictates the
character set that the object will enumerate through.

Syntax:
```js
	var charset = 'abcd123';
	var eString = new EnumString.EnumString(charset);
```

## EnumString.prototype.setIndex
Sets the current index of enumeration to a specified number.

Syntax:
```js
	eString.setIndex(2);
```

## EnumString.prototype.getIndex
Returns the current enumeration index.

Syntax:
```js
	eString.setIndex(4);
	console.log(eString.getIndex()); // Logs "4"
```

## EnumString.prototype.getString
Gets the string identified by the current enumeration index.

Syntax:
```js
	// Assuming the charset 'abcd123'
	eString.setIndex(0);
	console.log(eString.getString()); // Logs 'a'
	eString.setIndex(1);
	console.log(eString.getString()); // Logs 'b'
	eString.setIndex(7);
	console.log(eString.getString()); // Logs 'aa'
```

## EnumString.prototype.nextString
Returns the string at `index` **then** increases index by one, for use in loops.

Syntax:
```js
	// Assuming the charset 'abcd123'
	eString.setIndex(3);
	for (var i = 0; i < 3; i++) {
		console.log(eString.nextString());
	}
	// Logs: d 1 2
```

## EnumString.prototype.prevString
Returns the string at `index` **then** decreases index by one, for use in loops.

Syntax:
```js
	// Assuming the charset 'abcd123'
	eString.setIndex(8);
	for (var i = 0; i < 3; i++) {
		console.log(eString.prevString());
	}
	// Logs: ab aa 3
```

## EnumString.prototype.getIndexFromStr
Gets the index that would correspond to the given string.

Syntax:
```js
	// Assuming the charset 'abcd123';
	console.log(eString.getIndexFromStr('a')); // Logs: '0'
	console.log(eString.getIndexFromStr('2')); // Logs: '5'
	console.log(eString.getIndexFromStr('aa')); // Logs: '7'
```

## EnumString.letters
A quality of life string containing upper and lowercase letters

Syntax:
```js
	console.log(EnumString.letters); // Logs: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
```

## EnumString.digits
A quality of life string containing numbers 0-9

Syntax:
```js
	console.log(EnumString.digits); // Logs: '0123456789'
```

## EnumString.symbols
A quality of life string containing some symbols

Syntax:
```js
	console.log(EnumString.symbols); // Logs: '`~!@#$%^&*()-_=+[{]}|\\;:\'",<.>/?'
```
