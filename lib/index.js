/*
 * Primary class for an enumeratable string.
 */
function EnumString (dict) {
	if (dict.constructor === Array) {
		this.dict = dict;
	} else if (dict.constructor === String) {
		this.dict = dict.split('');
	} else {
		console.error('`dict` must be of type "Array" or "String"');
	}
	this.index = 0;
}

// The index variable can be modified directly but this looks cleaner
EnumString.prototype.setIndex = function (i) {
	this.index = (i >= 0) ? i : 0;
};

EnumString.prototype.getIndex = function (i) {
	return this.index;
};

EnumString.prototype.getString = function () {
	if (this.index < 0) {
		this.index = 0;
		return this.dict[0];
	} else {
		var out = '';
		var i = 0;
		while (true) {
			var mag = Math.pow(this.dict.length, i + 1);
			var prevMag = Math.pow(this.dict.length, i);
			var sumPrevMags = (mag - 1) / (this.dict.length - 1);
			if (this.index + 1 >= sumPrevMags) {
				var ind = Math.floor(((this.index - (sumPrevMags - 1)) % mag) / prevMag);
				out = this.dict[ind] + out;
			} else {
				break;
			}
			i += 1;
		}
		return out;
	}
};

// QOL function to get next string
EnumString.prototype.nextString = function () {
	var out = this.getString();
	this.index++;
	return out;
};

// QOL function to get previous string
EnumString.prototype.prevString = function () {
	var out = this.getString();
	this.setIndex(this.index - 1);
	return out;
};

// Function to get index from current string
EnumString.prototype.getIndexFromStr = function (str) {
	var chars = str.split('').reverse();
	var ind = 0;
	for (var i = 0; i < chars.length; i++) {
		var loc = this.dict.indexOf(chars[i]);
		if (loc === -1) {
			console.error('`str` contains characters outside of the dictionary, returning -1');
			return -1;
		} else {
			ind += (loc + 1) * Math.pow(this.dict.length, i);
		}
	}
	return ind - 1;
};

exports.EnumString = EnumString;
exports.letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
exports.digits = '0123456789';
exports.symbols = '`~!@#$%^&*()-_=+[{]}|\\;:\'",<.>/?';
